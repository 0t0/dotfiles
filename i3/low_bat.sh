#!/bin/bash

bat_percent=$(acpi -b| grep -P -o '[0-9]+(?=%)')
bat_status=$(acpi -b| grep -c 'Charging')
bat_full=$(acpi -b | grep -c "Full")
if [ $bat_status -eq 0 -a $bat_percent -le 5 ]; then
			  exec systemctl suspend	
fi
if [ $bat_status -eq 0 -a $bat_percent -le 10 ]; then
				exec /usr/bin/notify-send -u critical -t 10000 "Low Battery, Please Charge"
fi
if [ $bat_status -eq 0 -a $bat_percent -le 15 ]; then
				exec /usr/bin/notify-send -u normal -t 10000 "Low Battery, Please Charge"
fi
if [ $bat_full -eq 1 -a $bat_percent -eq 98 ]; then
				exec notify-send "Full Charge"
fi
