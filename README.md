![i3-wm](screenshot/lap.png)
![i3-wm](screenshot/lock.png)

## Changing colorscheme
![Colorscheme](screenshot/colorscheme.gif)

## Rofi - powermenu
![Powermenu-1](screenshot/powermenu.png)  ![Powermenu-1](screenshot/powermenu-confirm.png)

![Powermenu-2](screenshot/powermenu-2.png)  ![Powermenu-2-confirm](screenshot/powermenu-2-confirm.png)

## Rofi theme - slate.rasi
![slate](screenshot/rofi.png)

## Rofi theme - menu.rasi
![rofi](screenshot/rofi-2.png)

## Rofi theme - appsmenu.rasi
![appsmenu](screenshot/rofi-3.png)
