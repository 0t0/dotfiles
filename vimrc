call plug#begin()
Plug 'ayu-theme/ayu-vim'
Plug 'scrooloose/nerdtree' 
Plug 'ryanoasis/vim-devicons'
Plug 'tmsvg/pear-tree'
Plug 'makerj/vim-pdf'
Plug 'junegunn/goyo.vim'
Plug 'rust-lang/rust.vim'
Plug 'sainnhe/gruvbox-material'
call plug#end()

"merdtree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

"colorscheme
set background=dark
colorscheme gruvbox-material

set tabstop=2
set number
"borders and tilde
highlight NonText ctermfg='Black'  
set enc=utf-8
set fillchars=

"key remap
map <C-n> :NERDTreeToggle<CR>
map <C-l> <C-w>l
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
autocmd! bufwritepost *.h make && sudo make install

"auto source
autocmd! bufwritepost .vimrc source %

"rust syntax
syntax enable
filetype plugin indent on
