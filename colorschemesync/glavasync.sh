#! /bin/bash

color15=#e7cbbf
color3=#8F718E

sed -i '/@fg:mix/d' $1
echo '#define COLOR @fg:mix('$color15', '$color3', clamp(d / GRADIENT, 0, 1))'  >> $1
